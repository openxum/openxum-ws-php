<?php

namespace openxum\kamisado;

// enums definition
abstract class GameType
{
    const SIMPLE = 0;
    const STANDARD = 1;
    const LONG = 2;
    const MARATHON = 3;
}

abstract class Phase
{
    const MOVE_TOWER = 0;
    const FINISH = 1;
}

abstract class Color
{
    const NONE = -1;
    const BLACK = 0;
    const WHITE = 1;
}

abstract class TowerColor
{
    const ORANGE = 0;
    const BLUE = 1;
    const PURPLE = 2;
    const PINK = 3;
    const YELLOW = 4;
    const RED = 5;
    const GREEN = 6;
    const BROWN = 7;
}

class Coordinates
{
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public $x;
    public $y;
}

/*class Moves
{
    public function __construct($pt, $l)
    {
        $this->playable_tower = $pt;
        $this->list = $l;
    }

    public $playable_tower;
    public $list;
}*/

class Move
{
    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public $from;
    public $to;
}

class State
{
    public function __construct($x, $y, $color)
    {
        $this->x = $x;
        $this->y = $y;
        $this->color = $color;
    }

    public $x;
    public $y;
    public $color;
}

class Engine
{

    // public methods
    public function __construct($t, $c)
    {
        $this->type = $t;
        $this->black_towers = [];
        for ($i = 0; $i < 8; $i++) {
            $this->black_towers[$i] = new State($i, 0, Engine::$colors[$i][0]);
        }
        $this->white_towers = [];
        for ($i = 0; $i < 8; $i++) {
            $this->white_towers[$i] = new State($i, 7, Engine::$colors[$i][7]);
        }
        $this->phase = Phase::MOVE_TOWER;
        $this->color = ($c == 0) ? Color::BLACK : Color::WHITE;
        $this->play_color = Color::NONE;
    }

    public function current_color()
    {
        return $this->color;
    }

    public function find_playable_tower($color)
    {
        $playable_tower = null;

        if ($this->play_color != Color::NONE) {
            $list = $this->get_towers($color);

            for ($i = 0; $i < 8; $i++) {
                if ($list[$i]->color == $this->play_color) {
                    $playable_tower = new Coordinates($list[$i]->x, $list[$i]->y);
                }
            }
        }
        return $playable_tower;
    }

    public function find_tower($coordinates, $color)
    {
        $tower = $this->find_tower2($coordinates, $color);

        return new State($tower->x, $tower->y, $tower->color);
    }

    public function get_black_towers()
    {
        return $this->black_towers;
    }

    public function get_current_towers()
    {
        return $this->color == Color::BLACK ? $this->black_towers : $this->white_towers;
    }

    public function get_phase()
    {
        return $this->phase;
    }

    public function get_play_color()
    {
        return $this->play_color;
    }

/*    public function get_possible_move_list()
    {
        $color = $this->current_color();
        $playable_tower = $this->find_playable_tower($color);

        if (is_null($playable_tower)) {
            $playable_tower = new Coordinates(mt_rand() % 8, $color == Color::BLACK ? 0 : 7);
        }
        return new Moves($playable_tower, $this->get_possible_moving_list(new State($playable_tower->x, $playable_tower->y, $color)));
    } */

    public function get_possible_moving_list($tower)
    {
        $list = [];
        $step = $tower->color == Color::BLACK ? 1 : -1;
        $limit = $tower->color == Color::BLACK ? 8 : -1;

        // column
        $line = $tower->y + $step;

        while ($line != $limit && $this->is_empty(new Coordinates($tower->x, $line))) {
            array_push($list, new Coordinates($tower->x, $line));
            $line += $step;
        }

        // right diagonal
        $col = $tower->x + 1;

        $line = $tower->y + $step;
        while ($line != $limit && $col != 8 && $this->is_empty(new Coordinates($col, $line))) {
            array_push($list, new Coordinates($col, $line));
            $line += $step;
            $col++;
        }

        // left diagonal
        $col = $tower->x - 1;
        $line = $tower->y + $step;
        while ($line != $limit && $col != -1 && $this->is_empty(new Coordinates($col, $line))) {
            array_push($list, new Coordinates($col, $line));
            $line += $step;
            $col--;
        }
        return $list;
    }

    public function get_towers($color)
    {
        return $color == Color::BLACK ? $this->black_towers : $this->white_towers;
    }

    public function get_white_towers()
    {
        return $this->white_towers;
    }

    public function is_finished()
    {
        return $this->phase == Phase::FINISH;
    }

    public function move($move)
    {
        $this->move_tower($move->from, $move->to);
    }

    public function pass($color)
    {
        $playable_tower = $this->find_playable_tower($color);
        $list = $this->get_possible_moving_list(new State($playable_tower->x, $playable_tower->y, $color));

        return empty($list);
    }

/*    public function select_move($list, $index)
    {
        return new Move($list->playable_tower, $list->list[$index]);
    } */

    public function winner_is()
    {
        if ($this->is_finished()) {
            return $this->color;
        } else {
            return null;
        }
    }

    // private methods
    private function change_color()
    {
        $this->color = $this->next_color($this->color);
    }

    private function is_empty($coordinates)
    {
        $found = false;
        $i = 0;

        while ($i < 8 && !$found) {
            if ($this->black_towers[$i]->x == $coordinates->x && $this->black_towers[$i]->y == $coordinates->y) {
                $found = true;
            } else {
                $i++;
            }
        }
        $i = 0;
        while ($i < 8 && !$found) {
            if ($this->white_towers[$i]->x == $coordinates->x && $this->white_towers[$i]->y == $coordinates->y) {
                $found = true;
            } else {
                $i++;
            }
        }
        return !$found;
    }

    private function move_tower($selected_tower, $selected_cell)
    {
        $tower = $this->find_tower2($selected_tower, $this->color);
        if (!is_null($tower)) {
            $tower->x = $selected_cell->x;
            $tower->y = $selected_cell->y;
        }
        if (($this->color == Color::BLACK && $tower->y == 7) || ($this->color == Color::WHITE && $tower->y == 0)) {
            $this->phase = Phase::FINISH;
        } else {
            $this->play_color = Engine::$colors[$tower->x][$tower->y];
            if (!$this->pass($this->next_color($this->color))) {
                $this->change_color();
            } else {
                $playable_tower = $this->find_playable_tower($this->next_color($this->color));

                $this->play_color = Engine::$colors[$playable_tower->x][$playable_tower->y];
                if ($this->pass($this->color)) {
                    $this->phase = Phase::FINISH;
                    $this->color = $this->next_color($this->color);
                }
            }
        }
    }

    private function next_color($color)
    {
        return $color == Color::WHITE ? Color::BLACK : Color::WHITE;
    }

    private function find_tower2($coordinates, $color)
    {
        $tower = null;
        $list = $color == Color::BLACK ? $this->black_towers : $this->white_towers;
        $found = false;
        $i = 0;

        while ($i < 8 && !$found) {
            if ($list[$i]->x == $coordinates->x && $list[$i]->y == $coordinates->y) {
                $tower = $list[$i];
                $found = true;
            } else {
                $i++;
            }
        }
        if (!$found) {
            error_log('not found tower');
        }
        return $tower;
    }

// private attributes
    public $color;
    public $type;

    public $black_towers;
    public $white_towers;
    public $play_color;

    public $phase;

// private constantes
    private static $colors = [
        [TowerColor::ORANGE, TowerColor::BLUE, TowerColor::PURPLE, TowerColor::PINK, TowerColor::YELLOW, TowerColor::RED, TowerColor::GREEN, TowerColor::BROWN],
        [TowerColor::RED, TowerColor::ORANGE, TowerColor::PINK, TowerColor::GREEN, TowerColor::BLUE, TowerColor::YELLOW, TowerColor::BROWN, TowerColor::PURPLE],
        [TowerColor::GREEN, TowerColor::PINK, TowerColor::ORANGE, TowerColor::RED, TowerColor::PURPLE, TowerColor::BROWN, TowerColor::YELLOW, TowerColor::BLUE],
        [TowerColor::PINK, TowerColor::PURPLE, TowerColor::BLUE, TowerColor::ORANGE, TowerColor::BROWN, TowerColor::GREEN, TowerColor::RED, TowerColor::YELLOW],
        [TowerColor::YELLOW, TowerColor::RED, TowerColor::GREEN, TowerColor::BROWN, TowerColor::ORANGE, TowerColor::BLUE, TowerColor::PURPLE, TowerColor::PINK],
        [TowerColor::BLUE, TowerColor::YELLOW, TowerColor::BROWN, TowerColor::PURPLE, TowerColor::RED, TowerColor::ORANGE, TowerColor::PINK, TowerColor::GREEN],
        [TowerColor::PURPLE, TowerColor::BROWN, TowerColor::YELLOW, TowerColor::BLUE, TowerColor::GREEN, TowerColor::PINK, TowerColor::ORANGE, TowerColor::RED],
        [TowerColor::BROWN, TowerColor::GREEN, TowerColor::RED, TowerColor::YELLOW, TowerColor::PINK, TowerColor::PURPLE, TowerColor::BLUE, TowerColor::ORANGE]
    ];

}