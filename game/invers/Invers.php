<?php

namespace openxum\invers;

// enums definition
abstract class GameType
{
    const STANDARD = 0;
}

abstract class Phase
{
    const PUSH_TILE = 0;
    const FINISH = 1;
}

abstract class Color
{
    const NONE = -1;
    const RED = 0;
    const YELLOW = 1;
}

abstract class Position
{
    const NONE = -1;
    const TOP = 0;
    const BOTTOM = 1;
    const LEFT = 2;
    const RIGHT = 3;
}

abstract class State
{
    const UNDEFINED = -1;
    const RED_FULL = 0;
    const YELLOW_FULL = 1;
    const RED_REVERSE = 2;
    const YELLOW_REVERSE = 3;
}

class Coordinates
{
    public function __construct($letter, $number)
    {
        $this->letter = $letter;
        $this->number = $number;
    }

    public $letter;
    public $number;
}

class Move
{
    public function __construct($color, $letter, $number, $position)
    {
        $this->color = $color;
        $this->letter = $letter;
        $this->number = $number;
        $this->position = $position;
    }

    public $color;
    public $letter;
    public $number;
    public $position;
}

class Moves
{
    public function __construct($right, $left, $top, $bottom)
    {
        $this->right = $right;
        $this->left = $left;
        $this->top = $top;
        $this->bottom = $bottom;
    }

    public $right;
    public $left;
    public $top;
    public $bottom;
}

class Engine
{

    // public methods
    public function __construct($t, $c)
    {
        $tile_color = State::RED_FULL;

        $this->type = $t;
        $this->color = $c;
        $this->phase = Phase::PUSH_TILE;
        $this->state = [];
        for ($i = 0; $i < 6; $i++) {
            $line = [];

            for ($j = 0; $j < 6; $j++) {
                array_push($line, $tile_color);
                $tile_color = $tile_color === State::RED_FULL ? State::YELLOW_FULL : State::RED_FULL;
            }
            array_push($this->state, $line);
            $tile_color = $tile_color === State::RED_FULL ? State::YELLOW_FULL : State::RED_FULL;
        }
        $this->redTileNumber = 1;
        $this->yellowTileNumber = 1;
    }

    public function current_color()
    {
        return $this->color;
    }

    public function get_different_color_number_of_free_tiles()
    {
        return ($this->redTileNumber === 2 || $this->yellowTileNumber === 2) ? 1 : 2;
    }

    public function get_free_tiles()
    {
        $free_colors = [];
        $index = 0;
        for ($i = 0; $i < $this->redTileNumber; $i++) {
            $free_colors[$index] = Color::RED;
            $index++;
        }
        for ($i = 0; $i < $this->yellowTileNumber; $i++) {
            $free_colors[$index] = Color::YELLOW;
            $index++;
        }
        return $free_colors;
    }

    public function get_phase()
    {
        return $this->phase;
    }

    public function get_possible_move_list()
    {
        $right = [];
        $left = [];
        $top = [];
        $bottom = [];
        $state = $this->color === Color::RED ? State::YELLOW_REVERSE : State::RED_REVERSE;

        for ($n = 0; $n < 6; $n++) {
            // RIGHT
            {
                $coordinates = new Coordinates('A', $n + 1);
                if ($this->get_tile_state($coordinates) !== $state) {
                    array_push($right, new Coordinates('X', $n + 1));
                }
            }
            // LEFT
            {
                $coordinates = new Coordinates('F', $n + 1);
                if ($this->get_tile_state($coordinates) !== $state) {
                    array_push($left, new Coordinates('X', $n + 1));
                }
            }
        }

        for ($l = 0; $l < 6; $l++) {
            // BOTTOM
            {
                $coordinates = new Coordinates(chr(ord('A') + $l), 1);
                if ($this->get_tile_state($coordinates) !== $state) {
                    array_push($bottom, new Coordinates(chr(ord('A') + $l), 0));
                }
            }
            // TOP
            {
                $coordinates = new Coordinates(chr(ord('A') + $l), 6);
                if ($this->get_tile_state($coordinates) !== $state) {
                    array_push($top, new Coordinates(chr(ord('A') + $l), 0));
                }
            }
        }
        return new Moves($right, $left, $top, $bottom);
    }

    public function get_possible_move_number($list)
    {
        return (count($list->top) + count($list->bottom) + count($list->left) + count($list->right)) * $this->get_different_color_number_of_free_tiles();
    }

    public function move($move)
    {
        if ($move->letter !== 'X') {
            $letter = $move->letter;

            if ($move->position === Position::TOP) {
                $state = $this->get_tile_state(new Coordinates($letter, 6));
                $out = $state === State::RED_FULL || $state === State::RED_REVERSE ? Color::RED : Color::YELLOW;
                for ($n = 5; $n >= 1; $n--) {
                    $destination = new Coordinates($letter, $n + 1);
                    $origin = new Coordinates($letter, $n);
                    $this->set_tile_state($destination, $this->get_tile_state($origin));
                }
                $this->set_tile_state(new Coordinates($letter, 1),
                    ($move->color === Color::RED ? State::RED_REVERSE : State::YELLOW_REVERSE));
            } else {
                $state = $this->get_tile_state(new Coordinates($letter, 1));
                $out = $state === State::RED_FULL || $state === State::RED_REVERSE ? Color::RED : Color::YELLOW;
                for ($n = 1; $n < 6; $n++) {
                    $destination = new Coordinates($letter, $n);
                    $origin = new Coordinates($letter, $n + 1);
                    $this->set_tile_state($destination, $this->get_tile_state($origin));
                }
                $this->set_tile_state(new Coordinates($letter, 6),
                    ($move->color === Color::RED ? State::RED_REVERSE : State::YELLOW_REVERSE));
            }
        } else {
            $number = $move->number;
            if ($move->position === Position::RIGHT) {
                $state = $this->get_tile_state(new Coordinates('A', $number));
                $out = $state === State::RED_FULL || $state === State::RED_REVERSE ? Color::RED : Color::YELLOW;
                for ($l = 0; $l < 5; $l++) {
                    $destination = new Coordinates(chr(ord('A') + $l), $number);
                    $origin = new Coordinates(chr(ord('A') + $l + 1), $number);
                    $this->set_tile_state($destination, $this->get_tile_state($origin));
                }
                $this->set_tile_state(new Coordinates('F', $number),
                    ($move->color === Color::RED ? State::RED_REVERSE : State::YELLOW_REVERSE));
            } else {
                $state = $this->get_tile_state(new Coordinates('F', $number));
                $out = $state === State::RED_FULL || $state === State::RED_REVERSE ? Color::RED : Color::YELLOW;
                for ($l = 4; $l >= 0; $l--) {
                    $destination = new Coordinates(chr(ord('A') + $l + 1), $number);
                    $origin = new Coordinates(chr(ord('A') + $l), $number);
                    $this->set_tile_state($destination, $this->get_tile_state($origin));
                }
                $this->set_tile_state(new Coordinates('A', $number),
                    ($move->color === Color::RED ? State::RED_REVERSE : State::YELLOW_REVERSE));
            }
        }
        if ($move->color === Color::RED) {
            $this->redTileNumber--;
        } else {
            $this->yellowTileNumber--;
        }
        if ($out === Color::RED) {
            $this->redTileNumber++;
        } else {
            $this->yellowTileNumber++;
        }
        if ($this->is_finished(Color::RED) || $this->is_finished(Color::YELLOW)) {
            $this->phase = Phase::FINISH;
        } else {
            $this->change_color();
        }
    }

    public function select_move($list, $index)
    {
        $N = count($list->top) + count($list->bottom) + count($list->left) + count($list->right);
        $N2 = $N * $this->get_different_color_number_of_free_tiles();

        if ($index >= $N) {
            $index -= $N;
            $color_index = 1;
        } else {
            $color_index = 0;
        }
        if ($index < count($list->top)) {
            $L = $list->top;
            $position = Position::TOP;
        } else if ($index < count($list->top) + count($list->bottom)) {
            $L = $list->bottom;
            $position = Position::BOTTOM;
            $index -= count($list->top);
        } else if ($index < count($list->top) + count($list->bottom) + count($list->left)) {
            $L = $list->left;
            $position = Position::LEFT;
            $index -= count($list->top) + count($list->bottom);
        } else {
            $L = $list->right;
            $position = Position::RIGHT;
            $index -= count($list->top) + count($list->bottom) + count($list->left);
        }
        return new Move($this->get_free_tiles()[$color_index], $L[$index]->letter, $L[$index]->number, $position);
    }

// private methods
    private function change_color()
    {
        $this->color = $this->next_color($this->color);
    }

    private function get_tile_state($coordinator)
    {
        $i = ord($coordinator->letter) - ord("A");
        $j = $coordinator->number - 1;

        return $this->state[$i][$j];
    }

    private function is_finished($color)
    {
        $state = ($color === Color::RED ? State::RED_FULL : State::YELLOW_FULL);
        $found = false;

        for ($n = 1; $n <= 6 && !$found; $n++) {
            for ($l = 0; $l < 6 && !$found; $l++) {
                $found = $this->get_tile_state(new Coordinates(chr(ord("A") + $l), $n)) === $state;
            }
        }
        return !$found;
    }

    private function next_color($color)
    {
        return $color == Color::RED ? Color::YELLOW : Color::RED;
    }

    private function set_tile_state($coordinator, $s)
    {
        $i = ord($coordinator->letter) - ord("A");
        $j = $coordinator->number - 1;

        $state[$i][$j] = $s;
    }

// private attributes
    public $color;
    public $type;

    public $phase;
    public $state;

    public $redTileNumber;
    public $yellowTileNumber;
}