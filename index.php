<?php

session_start();

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

use openxum\kamisado;
use openxum\invers;

include 'game/kamisado/Kamisado.php';
include 'game/invers/Invers.php';

$app = new \Slim\Slim();

header('Access-Control-Allow-Origin: http://127.0.0.1:3000');
header('Access-Control-Allow-Credentials: true');

$app->options('/(:name+)', function() use($app) {
    $response = $app->response();
    $app->response()->status(200);
    $response->header('Access-Control-Allow-Headers', 'Content-Type, X-Requested-With, X-authentication, X-client');
    $response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
 });

$app->post('/openxum/create', function () use ($app) {
    $req = $app->request();
    $game = $req->params('game');
    $color = $req->params('color');
    $login = $req->params('login');

    $id = $login . md5(uniqid($login, true));
    $response = [
        "id" => $id
    ];

    if ($game === "kamisado") {
        $_SESSION[$id] = serialize(new kamisado\Engine(0, $color));
    } else if ($game === "invers") {
        $_SESSION[$id] = serialize(new invers\Engine(0, $color));
    }
    echo json_encode($response);
});

$app->put('/openxum/move/', function () use ($app) {
    $req = $app->request();
    $game = $req->params('game');
    $id = $req->params('id');

    if (isset($_SESSION[$id])) {
        $engine = unserialize($_SESSION[$id]);
        if ($game === "kamisado") {
            $from = json_decode($req->params('from'));
            $to = json_decode($req->params('to'));
            $engine->move(new kamisado\Move($from, $to));
        } else if ($game === "invers") {
            $move = json_decode($req->params('move'));
            $engine->move(new invers\Move($move->color, $move->letter, $move->number, $move->position));
        }
        $_SESSION[$id] = serialize($engine);
        echo json_encode([ 'color' => $engine->current_color() ]);
    } else {
        error_log('unserialization error');
    }
});

$app->get('/openxum/move/', function () use ($app) {
    $req = $app->request();
    $game = $req->params('game');
    $id = $req->params('id');

    if (isset($_SESSION[$id])) {
        $color = $req->params('color');
        $engine = unserialize($_SESSION[$id]);
        $response = [];
        if ($game === "kamisado") {
            $playable_tower = $engine->find_playable_tower($color);
            if (is_null($playable_tower)) {
                $playable_tower = new kamisado\Coordinates(mt_rand() % 8, $color == kamisado\Color::BLACK ? 0 : 7);
            }
            $list = $engine->get_possible_moving_list(new kamisado\State($playable_tower->x, $playable_tower->y, $color));
            if (count($list) > 0) {
                $response = ['from' => $playable_tower, 'to' => $list[mt_rand() % count($list)]];
            }
        } else if ($game === "invers") {
            $list = $engine->get_possible_move_list();
            $letter = 'X';
            $number = -1;
            $color = $engine->get_free_tiles()[mt_rand() % 2];

            do {
                $position = mt_rand() % 4;
                if ($position === invers\Position::TOP) {
                    $l = $list->top;
                } else if ($position === invers\Position::BOTTOM) {
                    $l = $list->bottom;
                } else if ($position === invers\Position::LEFT) {
                    $l = $list->left;
                } else if ($position === invers\Position::RIGHT) {
                    $l = $list->right;
                } else {
                    $l = [];
                }
            } while (count($l) === 0);
            if ($position === invers\Position::TOP || $position === invers\Position::BOTTOM) {
                $letter = $l[mt_rand() % count($l)]->letter;
            } else {
                $number = $l[mt_rand() % count($l)]->number;
            }
            $response = ['color' => $color, 'letter' => $letter, 'number' => $number, 'position' => $position];
        }
        echo json_encode($response);
    } else {
        error_log('unserialization error');
    }
});

$app->run();
